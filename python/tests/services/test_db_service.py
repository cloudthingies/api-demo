from services import db_service

from unittest.mock import patch, MagicMock
from decimal import Decimal


@patch("services.db_service._gsi_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_transaction_items_for_account__deserialized_data_for_each_transaction_once(deserialize_mock, query_mock):
    query_mock.return_value = [{'SenderAccountId': {'S': '111111'}, 'ReceiverAccountId': {'S': '000000'}, 'Amount': {'N': '999'}, 'TransactionId': {'S': '1234'}}, {'SenderAccountId': {'S': '111111'}, 'ReceiverAccountId': {'S': '000000'}, 'Amount': {'N': '50'}, 'TransactionId': {'S': '8888'}}]

    db_service._get_transaction_items_for_account("fake_account_id", False)

    assert deserialize_mock.call_count == 4


@patch("services.db_service._gsi_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_transaction_items_for_account__handles_empty_responses(deserialize_mock, query_mock):
    query_mock.return_value = []

    db_service._get_transaction_items_for_account("fake_account_id", False)

    assert query_mock.call_count == 2
    assert deserialize_mock.call_count == 0


@patch("services.db_service._gsi_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_transaction_items_for_account__queries_with_gsi(deserialize_mock, query_mock):
    db_service._get_transaction_items_for_account("fake_account_id", False)

    assert query_mock.call_count == 2


@patch("services.db_service._pm_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_account_items_for_customer__deserialized_data_for_each_account_once(deserialize_mock, query_mock):
    query_mock.return_value = [{'Balance': {'N': '50'}, 'AccountId': {'S': '000000'}, 'CustomerId': {'S': '123654'}}, {'Balance': {'N': '-50'}, 'AccountId': {'S': '111111'}, 'CustomerId': {'S': '123654'}}]

    db_service._get_account_items_for_customer("fake_customer_id", False)

    assert deserialize_mock.call_count == 2


@patch("services.db_service._pm_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_account_items_for_customer__handles_empty_response(deserialize_mock, query_mock):
    query_mock.return_value = []

    db_service._get_account_items_for_customer("fake_customer_id", False)

    assert deserialize_mock.call_count == 0


@patch("services.db_service._pm_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_account_items_for_customer__queries_with_pm(deserialize_mock, query_mock):
    query_mock.return_value = []

    db_service._get_account_items_for_customer("fake_customer_id", False)

    assert query_mock.call_count == 1


@patch("services.db_service._pm_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_customer_item__queries_with_pm(deserialize_mock, query_mock):
    query_mock.return_value = []

    db_service._get_customer_item("fake_customer_id", False)

    assert query_mock.call_count == 1


@patch("services.db_service._pm_query_dynamodb_items")
@patch("services.db_service._remove_dynamodb_typing")
def test__get_customer_item__deserializes_customer_item(deserialize_mock, query_mock):
    query_mock.return_value = [{'Surname': {'S': 'Builder'}, 'CustomerId': {'S': '123654'}, 'Name': {'S': 'Bob'}}]

    db_service._get_customer_item("fake_customer_id", False)

    assert deserialize_mock.call_count == 1


@patch("services.db_service._pm_query_dynamodb_items")
def test__get_customer_item__returns_dict(query_mock):
    query_mock.return_value = [{'Surname': {'S': 'Builder'}, 'CustomerId': {'S': '123654'}, 'Name': {'S': 'Bob'}}]
    result = db_service._get_customer_item("fake_customer_id", False)

    assert type(result) == dict


@patch("utilities.configuration_utility.get_account_table_name")
@patch("uuid.uuid4")
@patch("services.db_service._put_dynamodb_item")
def test__create_account__puts_correct_data_in_db(put_mock, uuid_mock, get_table_name_mock):

    customer_id = "fake_id"
    uuid = "fake_uuid"
    table_name = "fake_table_name"

    mock_uuid_object = MagicMock()
    uuid_mock.return_value = mock_uuid_object
    mock_uuid_object.hex = uuid

    get_table_name_mock.return_value = "fake_table_name"

    db_service.create_account(customer_id)

    print(put_mock.call_list)

    put_mock.assert_called_with(table_name, {"CustomerId": customer_id, "Balance": Decimal('0'), "AccountId": uuid})
