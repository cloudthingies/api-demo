from models import Response

from unittest.mock import MagicMock
import json

def test__response__init_default_ok():
    response = Response()

    assert response.status_code == 501
    assert not response.is_base_64_encoded
    assert response.headers == {"Content-Type": "application/json"}
    assert json.loads(response.body) == {"message": "Not Implemented", "data": []}


def test__response__init_cors_true_ok():
    response = Response(enable_cors=True)

    assert response.headers == {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"}


def test__set_ok__handles_exception():
    response = Response()
    response.set_ok(MagicMock())  #  Throw in a MagicMock to cause exception in set_ok

    print(response.get_json_response())
    print(response.body)

    assert response.status_code == 500
    assert type(response.body) == str
    assert json.loads(response.body) == {"message": "Internal Server Error", "data": []}


# def test__get_json_response__handles_exception():
#     response = Response()
#     response.set_ok(MagicMock()) #  Throw in a MagicMock to cause exception in set_ok
#     json_response = response.get_json_response()

#     # print(response.get_json_response())
#     # print(response.body)

#     assert type(json_response) == str
#     print(json.loads(json_response))
#     assert json.loads(json_response).get("statusCode") == 500
#     assert json.loads(json_response).get("body") == {"message": "Internal Server Error", "data": []}