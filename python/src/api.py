from models import Response
from services import db_service

import json


def get_customer(event, context):
    print(event)
    response = Response()
    try:
        customer_id = event["pathParameters"]["customer_id"]
        print(customer_id)
        data = db_service.get_customer(customer_id)
        if data:
            print(f"data exists as: {data}")
            response.set_ok(data)
        else:
            print("data doesnt exist")
            response.set_empty()
    #  Wont bother with granularity of error since everything will be a 500 to user anyway
    except Exception as e:
        print(e)
        response.set_internal_error()
    return response.get_json_response()


def put_account(event, context):
    print(event)
    response = Response()
    try:
        customer_id = event["pathParameters"]["customer_id"]
        print(f"{customer_id=}")
        initial_balance = _validate_initial_balance(event)
        print(f"{initial_balance=}")
        if initial_balance is not None and db_service.customer_exists(customer_id):
            print("Creating account....")
            account_id = db_service.create_account(customer_id)
            print(f"Created account {account_id}")
            # TODO call put for transaction if initialCredit
            if initial_balance:
                db_service.create_transaction("init", account_id, initial_balance)
            response.set_ok([])
        elif initial_balance is None:
            print("Missing initial balance!")
            response.set_invalid_request()
        else:
            print("Customer doesnt exist!")
            response.set_empty()
    #  Wont bother with granularity of error since everything will be a 500 to user anyway
    except Exception as e:
        print(e)
        response.set_internal_error()
    return response.get_json_response()


def _validate_initial_balance(event):
    print("Validating initialBalance...")
    try:
        initial_balance = json.loads(event["body"])["initialCredit"]
        return initial_balance if type(initial_balance) == int else None
    except Exception:
        print("Malformed json request")
        return None
