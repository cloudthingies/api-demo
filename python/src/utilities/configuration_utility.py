import os


def get_customer_table_name():
    return os.environ.get("CUSTOMER_TABLE_NAME")


def get_account_table_name():
    return os.environ.get("ACCOUNT_TABLE_NAME")


def get_transactions_table_name():
    return os.environ.get("TRANSACTIONS_TABLE_NAME")
