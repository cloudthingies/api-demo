from utilities.decimal_encoder import DecimalEncoder

import json


class Response:
    def __init__(self, enable_cors: bool = False):
        self.is_base_64_encoded = False
        self.status_code = 501
        self.headers = {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"} if enable_cors else {"Content-Type": "application/json"}
        self.body = json.dumps({"message": "Not Implemented", "data": []}, cls=DecimalEncoder)

    def set_internal_error(self):
        self.status_code = 500
        self.body = json.dumps({"message": "Internal Server Error", "data": []}, cls=DecimalEncoder)

    #  Always return 403 to give limited information to requester
    def set_empty(self):
        self.status_code = 403
        self.body = json.dumps({"message": "Forbidden", "data": []}, cls=DecimalEncoder)

    def set_ok(self, data):
        self.status_code = 200
        try:
            self.body = json.dumps({"message": "Success", "data": data}, cls=DecimalEncoder)
        except Exception as e:
            print(e)
            self.set_internal_error()

    def set_invalid_request(self):
        self.status_code = 400
        self.body = json.dumps({"message": "Bad Request", "data": []}, cls=DecimalEncoder)

    def get_json_response(self) -> str:
        response = (
            {
                "isBase64Encoded": self.is_base_64_encoded,
                "statusCode": self.status_code,
                "headers": self.headers,
                "body": self.body
            }
        )
        return response
