import json
from decimal import Decimal


class DecimalEncoder(json.JSONEncoder):
    '''
        Only seem to work when encoding dictionaries, and not on stand-alone Decimal
    '''
    def default(self, o):
        if isinstance(o, Decimal):
            return str(o)
        return super().default(self, o)
