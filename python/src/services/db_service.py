from utilities import configuration_utility

import boto3
import uuid
import logging
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer
from decimal import Decimal

LOGGER = logging.getLogger(__name__)


def get_customer(customer_id: str, consistent_read: bool = False) -> dict:
    print(f"get_customer called with {customer_id=}, {consistent_read=}")
    customer = _get_customer_item(customer_id, consistent_read)
    print(f"_get_customer_item returned {customer}")
    if not customer:
        print("customer was None!")
        return None
    print("Calling _get_account_items_for_customer...")
    accounts = _get_account_items_for_customer(customer_id, consistent_read)
    print(f"_get_account_items_for_customer returned {accounts}")
    for account in accounts:
        print(f"Calling _get_transaction_items_for_account for account {account}...")
        transactions = _get_transaction_items_for_account(account["AccountId"])
        print(f"_get_transaction_items_for_account returned {transactions}")
        account["Transactions"] = transactions
    customer["Accounts"] = accounts
    return customer


def customer_exists(customer_id: str) -> bool:
    return _get_customer_item(customer_id, False) is not None


def create_account(customer_id: str) -> str:
    print(f"create_account called with {customer_id=}")
    account_id = uuid.uuid4().hex
    print(f"Generated account_id {account_id}")
    data = {
        "CustomerId": customer_id,
        "Balance": Decimal('0'),
        "AccountId": account_id
    }
    print(f"data for PUT: {data}")
    try:
        _put_dynamodb_item(configuration_utility.get_account_table_name(), data)
        return account_id
    except Exception as e:
        print(e)
        return None


def create_transaction(sender_id: str, receiver_id: str, amount: float) -> str:
    if (not customer_exists(sender_id) or not customer_exists(receiver_id)) and sender_id != "init":
        return None
    transaction_id = uuid.uuid4().hex
    data = {
        "SenderAccountId": sender_id,
        "Balance": Decimal(str(amount)),
        "ReceiverAccountId": receiver_id,
        "TransactionId": transaction_id
    }
    try:
        _put_dynamodb_item(configuration_utility.get_transactions_table_name(), data)
        return transaction_id
    except Exception as e:
        print(e)
        return None


def _put_dynamodb_item(table_name: str, item: dict) -> dict:
    client = _get_client()
    dynamodb_item = _add_dynamodb_typing(item)
    response = client.put_item(
        TableName=table_name,
        Item=dynamodb_item
    )
    print(f"response from _put_dynamodb_item: {response}")
    return response


def _get_customer_item(customer_id: str, consistent_read: bool = False) -> dict:
    print(f"_get_customer_item called with {customer_id=}, {consistent_read=}")
    table_name = configuration_utility.get_customer_table_name()
    print(f"{table_name=}")
    customer_data = _pm_query_dynamodb_items(table_name, "CustomerId", customer_id, consistent_read)
    print(f"_get_customer_item got {customer_data=}")
    return _remove_dynamodb_typing(customer_data[0]) if len(customer_data) > 0 else None


def _get_account_items_for_customer(customer_id: str, consistent_read: bool = False) -> dict:
    print(f"_get_account_items_for_customer called with {customer_id=}, {consistent_read=}")
    table_name = configuration_utility.get_account_table_name()
    accounts_data = _pm_query_dynamodb_items(table_name, "CustomerId", customer_id, consistent_read)
    print(f"_get_account_items_for_customer got {accounts_data=}")
    return [_remove_dynamodb_typing(account) for account in accounts_data]


def _get_transaction_items_for_account(account_id: str, consistent_read: bool = False) -> dict:
    print(f"_get_transaction_items_for_account called with {account_id=}, {consistent_read=}")
    table_name = configuration_utility.get_transactions_table_name()

    transactions_outgoing = _gsi_query_dynamodb_items(table_name, "SenderAccountId", account_id, "SenderAccountId-Index", consistent_read)
    transactions_outgoing = [_remove_dynamodb_typing(transaction) for transaction in transactions_outgoing]
    print(f"{transactions_outgoing=}")

    transactions_incoming = _gsi_query_dynamodb_items(table_name, "ReceiverAccountId", account_id, "ReceiverAccountId-Index", consistent_read)
    transactions_incoming = [_remove_dynamodb_typing(transaction) for transaction in transactions_incoming]
    print(f"{transactions_incoming=}")

    transactions: list = []
    transactions.extend(transactions_outgoing)
    transactions.extend(transactions_incoming)
    print(f"{transactions=}")
    return transactions


def _get_client(session=None, region="eu-west-1"):
    if session is None:
        session = boto3.Session()
    return session.client("dynamodb", region_name=region)


def _get_paginator(operation: str, client=None):
    if client is None:
        client = _get_client()
    #  Dont catch exception from get_paginator for invalid operations since it will crash anyway. boto3 errors are clear as it is.
    paginator = client.get_paginator(operation)
    return paginator


def _pm_query_dynamodb_items(table_name: str, key: str, val: str, consistent_read: bool) -> dict:
    print(f"_pm_query_dynamodb_item for {table_name} started!")
    paginator = _get_paginator("query")
    response_iterator = paginator.paginate(
        TableName=table_name,
        Select="ALL_ATTRIBUTES",
        ConsistentRead=consistent_read,
        KeyConditionExpression=f"{key} = :customer_id",
        ExpressionAttributeValues={":customer_id": {"S": val}}
    )
    items = []
    for item in response_iterator:
        items.extend(item["Items"])
    print(f"{items=}")
    return items


def _gsi_query_dynamodb_items(table_name: str, key: str, val: str, index_name: str, consistent_read: bool) -> dict:
    print(f"_gsi_query_dynamodb_item for {table_name} started!")
    paginator = _get_paginator("query")
    response_iterator = paginator.paginate(
        IndexName=index_name,
        TableName=table_name,
        Select="ALL_ATTRIBUTES",
        ConsistentRead=consistent_read,
        KeyConditionExpression=f"{key} = :customer_id",
        ExpressionAttributeValues={":customer_id": {"S": val}}
    )
    items = []
    for item in response_iterator:
        items.extend(item["Items"])
    print(f"{items=}")
    return items


def _remove_dynamodb_typing(dynamodb_data: dict) -> dict:
    print(f"_remove_dynamodb_typing deserializing {dynamodb_data}")
    deserializer = TypeDeserializer()
    deserialized_data = {k: deserializer.deserialize(v) for k, v in dynamodb_data.items()}
    print(f"Deserialized data: {dynamodb_data}")
    return deserialized_data


def _add_dynamodb_typing(dict: dict) -> dict:
    serializer = TypeSerializer()
    serialized_data = {k: serializer.serialize(v) for k, v in dict.items()}
    return serialized_data
