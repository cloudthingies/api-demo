from utilities.decimal_encoder import DecimalEncoder

import json
from decimal import Decimal


def test__decimalencoder__handles_decimal_type():
    decimal = {"asd": Decimal('50.34534')}

    result = json.dumps(decimal, cls=DecimalEncoder)

    assert result == '{"asd": "50.34534"}'
    assert type(result) == str